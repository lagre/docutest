.. docutest documentation master file, created by
   sphinx-quickstart on Sat May 19 13:35:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

"docutest", a test repo by Linus
====================================

Toctree
-------

.. toctree::
   :glob:
   :maxdepth: 0
   *

Graphviz!
===============

.. graphviz::

   digraph foo {
      "bar" -> "baz" -> "bajs";
   }
   
Build status
------------
.. image:: https://readthedocs.org/projects/docutest9000/badge/?version=latest
